## WebAssembly Calling Card (WACC) Viewer

_WACC Viewer lets you view & marvel at the wonder of WebAssembly Calling Cards!_

Find out more at: <https://wacc.rancidbacon.com/>

Download binaries from: <https://rancidbacon.itch.io/wacc-viewer>

![Screenshot of a heart WACC rendered on 2D texture & on a flag in a 3D world](wacc-viewer-v0.1.0-screenshot-flag-heart--cropped.png)

A demonstration project for the [WASM Engine for Godot](https://gitlab.com/RancidBacon/godot-wasm-engine) addon.

----

Brought to you by RancidBacon.com
