extends Camera

onready var OBJ_SOFT_BODY_FLAG: SoftBody = self.get_tree().root.find_node("SoftBodyFlag", true, false)

func _ready():

    pass

    # For some reason these attachments keep getting changed/reset which makes
    # the flag be attached in a completely incorrect way.

##
## This was a hack to try to get some movement but I think it causes issues
## due to it being intended to be used for internal pressure of enclosed meshes.
##
#    randomize()
#
#    while true:
#        yield(get_tree().create_timer(15.0), "timeout")
#
#        OBJ_SOFT_BODY_FLAG.set("pressure_coefficient", rand_range(-0.1, 0.1))
