extends Control

onready var UI_FILE_OPEN_DIALOG: FileDialog = get_tree().root.find_node("FileOpenDialog", true, false)

func _ready():

    # Attempt to semi-workaround default directory issues...
    UI_FILE_OPEN_DIALOG.current_dir = OS.get_system_dir(OS.SYSTEM_DIR_DOWNLOADS)
    UI_FILE_OPEN_DIALOG.current_path = OS.get_system_dir(OS.SYSTEM_DIR_DOWNLOADS).plus_file("/")


func _on_ButtonOpenFileDialog_pressed() -> void:
    UI_FILE_OPEN_DIALOG.popup_centered_ratio()
