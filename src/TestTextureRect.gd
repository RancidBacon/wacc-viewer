extends TextureRect

onready var UI_WACC_VIEWER: Control = self.get_tree().root.find_node("WaccViewer2D", true, false)
onready var UI_WACC_VIEW_3D: Spatial = self.get_tree().root.find_node("SimpleWaccView3D", true, false)
onready var UI_WACC_VIEW_3D_FLAG: Spatial = self.get_tree().root.find_node("WaccViewerFlag3D", true, false)


func _ready():

    self.add_child(UI_WACC_VIEWER.get_texture_rect())


func _on_ViewportContainer_ready() -> void:

    # Note: See note in `SimpleWaccViewer3D.gd` for why this can't be in
    #       our `_ready()` method.

    UI_WACC_VIEW_3D.wacc_texture = self.get_child(0).texture

    UI_WACC_VIEW_3D_FLAG.wacc_texture = self.get_child(0).texture

### Doing it this way leads to a resource leak
##
##    UI_WACC_VIEW_3D_FLAG.wacc_texture = UI_WACC_VIEWER.get_texture_rect().texture
##
### And then doing this to try to fix it leads to a crash:
##
## func _on_TestTextureRect_tree_exiting() -> void:
##
##   # Crashes (with some combo of these).
##    UI_WACC_VIEW_3D_FLAG.wacc_texture.unreference()
##    UI_WACC_VIEW_3D_FLAG.wacc_texture = null
##
