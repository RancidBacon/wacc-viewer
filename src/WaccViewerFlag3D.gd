extends Spatial

onready var OBJ_SOFT_BODY_FLAG: SoftBody = self.get_tree().root.find_node("SoftBodyFlag", true, false)


# Note: Use of export/setting here seems to require that it doesn't get used
#       until a `ViewportContainer` parent of an instanced scene emits a
#       `ready` signal.
#
#       Otherwise the instanced scene doesn't seem to be completely loaded
#       so the property setting results in a null value error. (So it seems
#       that the `ready` signal child propagation doesn't work in the same
#       way if there are child nodes not directly in the scene?)
#
#       See `TestTextureRect._on_ViewportContainer_ready()` for example use.
#
export var wacc_texture: Texture setget _set_wacc_texture

func _set_wacc_texture(the_texture):

    wacc_texture = the_texture

    #
    # NOTE: We are applying this texture to a `SoftBody` instance which does *not*
    #       have the same structure as e.g. `CSGBox`.
    #
    #
    #       It seems the IDE doesn't entirely accurately display the manner in
    #       which `SoftBody` is actually implemented in the inspector which can
    #       be confusing and/or misleading.
    #
    #       `SoftBody` inherits from `MeshInstance` which *has* a `mesh` property
    #       which is a `Mesh` but the `mesh` property is *actually* an `ArrayMesh`
    #       which inherits from `Mesh`.
    #
    #
    #       But perhaps most important of all, `Mesh` (and thus `ArrayMesh` also does)
    #       has a method named *`surface_get_material()`* which is *not* the
    #       same as the *very* similiarly named method *`get_surface_material()`*
    #       which is present in `MeshInstance` and thus *also* `SoftBody`. *phew*
    #
    #
    #       Don't get the two methods confused!
    #
    #          * `Mesh`/`ArrayMesh` has `surface_get_material()`.
    #
    #          * `MeshInstance`/`SoftBody` has `get_surface_material()`.
    #
    #          * NOTE: One method has "get" as first word, the other has "surface"!
    #
    #
    #       This is important to note, because if you set the `Material` of the
    #       `mesh` property then e.g. `<SoftBody>.get_surface_material(0)` will
    #       *always* return null!
    #
    #
    #       To get the `Material` used in the `mesh` property (in the `MeshInstance`
    #       inspector section) you *must* use e.g. `<SoftBody>.mesh.surface_get_material(0)`.
    #
    #       Don't ask me how long it took to figure this out...
    #
    #
    #       All of this is compounded by the debugger never displaying the
    #       materials in the inspector AFAICT.
    #
    #       It's also made less obvious because the docs don't show inherited
    #       methods in the docs for classes which inherit them.
    #

    var sgm = OBJ_SOFT_BODY_FLAG.mesh.surface_get_material(0)

    # Note: `surface_get_material()` *not* `get_surface_material()`! OMG!!!


    # Note: The following also requires, e.g.:
    #
    #       <SoftBody>.cast_shadow = GeometryInstance.SHADOW_CASTING_SETTING_DOUBLE_SIDED
    #

    sgm.albedo_texture = wacc_texture

    # Note: Required before `params_use_alpha_scissor` for unknown reasons
    #       otherwise no shadow ever appears.
    yield(VisualServer, "frame_post_draw")

    sgm.params_use_alpha_scissor = true
