extends HBoxContainer

onready var UI_PARENT: Control = self.find_parent("DisplayDemos3D")

onready var UI_VIEW_SIMPLE_3D: Spatial = UI_PARENT.find_node("SimpleWaccView3D", true, false)
onready var UI_VIEW_FLAG_3D: Spatial = UI_PARENT.find_node("WaccViewerFlag3D", true, false)

func _ready():
    pass


func _on_ButtonCube_toggled(button_pressed: bool) -> void:


    if button_pressed:
        UI_VIEW_SIMPLE_3D.visible = true
        $ButtonFlag.pressed = false
    else:
        UI_VIEW_SIMPLE_3D.visible = false
        if not $ButtonFlag.pressed:
            $ButtonFlag.pressed = true



func _on_ButtonFlag_toggled(button_pressed: bool) -> void:


    if button_pressed:
        UI_VIEW_FLAG_3D.visible = true
        $ButtonCube.pressed = false
    else:
        UI_VIEW_FLAG_3D.visible = false
        if not $ButtonCube.pressed:
            $ButtonCube.pressed = true
