extends Reference

class_name WaccLoader

var wasm = load("res://addons/wasm-engine/WasmEngine.gd") # TODO: Handle properly
var wasm_engine
var module


var wacc_fn # Note: Since we don't use it directly this is probably unnecessary.


func _init() -> void:

    # TODO: Share this more?
    self.wasm_engine = wasm.WasmEngine.new()

    if not self.wasm_engine:
        # TODO: Display error.
        push_error("Could not load WasmEngine.")
        return


func _is_valid_wacc(wasm_module) -> bool:

    if wasm_module.imports.size() > 0:
        push_warning("Imports are not supported in a WACC file.")
        return false

    for export_index in range(wasm_module.exports.size()):
        var current_export = wasm_module.exports.get_index(export_index)

        if current_export.type == wasm.ExternKind.WASM_EXTERN_FUNC and \
            current_export.name == "wacc":
                wacc_fn = current_export
                # TODO: Also verify signature matches.
                return true

    return false


func _load_valid_wacc(file_path: String):

    var wasm_module = wasm_engine.load_wasm_from_file(file_path)

    if not wasm_module:
        push_error("Failed to load `.wasm` file.")
        return

    if not self._is_valid_wacc(wasm_module):
        push_error("Not a valid WACC.")
        return

    return wasm_module
