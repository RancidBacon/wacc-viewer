extends Control

func coord_to_int(the_coord: Vector2) -> int:
    return int(the_coord.x) << 24 | int(the_coord.y) << 16

func color_to_int(the_color: Color) -> int:
    return int(round(the_color.r * 31)) << 10 | int(round(the_color.g * 31)) << 5 | int(round(the_color.b * 31)) << 0 | int(round(the_color.a)) << 15


func _ready():

    for current_child in $"VBoxContainer/HBoxContainer/Control".get_children():
        var current_polygon: Polygon2D = current_child

        var the_vals = []

        for current_coord in current_polygon.polygon:

            var current_color: Color = current_polygon.color

            # TODO: Support per vertex color.
            if current_polygon.vertex_colors:
                current_color = current_polygon.vertex_colors[the_vals.size()]

            var the_value: int = self.coord_to_int(current_coord) | self.color_to_int(current_color)

            the_vals.append("0x%08x" % the_value)

        print("    %s, %s, %s, // %s" % [the_vals[0], the_vals[1], the_vals[2], current_polygon.name])



# Hacky helpers to print out colour sequences for per-frame colour replacement lookups.
func __print_color_sequences():

    var colour0: Color = Color("#654ff0")
    print(" ")
    for i in range(5):
        print("0x%04x" % self.color_to_int(colour0.darkened(i * 0.15)))


    var color0: Color = Color("#f092b4")
    var color1: Color = Color("#f0518c")
    print(" ")
    for i in range(0, 5):
        print("0x%04x," % self.color_to_int(color0.linear_interpolate(color1, i * 0.2)))
