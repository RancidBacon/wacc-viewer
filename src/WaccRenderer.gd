extends Control

var vals: Array = [0x0000ffff, 0xffffffff, 0x00ffffff]

var wacc_module = null setget set_wacc_module


const _DRAW_RIGHT_NOW_TIME_ELAPSED_VALUE: float = 1000.0
func set_wacc_module(the_wacc_module):

    wacc_module = the_wacc_module

    self._reset_state()

    self._elapsed_time_seconds = _DRAW_RIGHT_NOW_TIME_ELAPSED_VALUE # Sure, we *could* calculate it...


func _ready():

    self._reset_state()


func coord_from_int(i: int):
    return Vector2((i >> 24) & 0xff, (i >> 16) & 0xff)

func color_from_int(i: int):
    return Color(((i >> 10) & 0b11111)/31.0, ((i >> 5) & 0b11111)/31.0 , ((i >> 0) & 0b11111)/31.0 , [0.0, 1.0][(i & 0x8000) >> 15])


const _FRAME_END_VALUE: int = 0x00000000

var _accumulated_triangles = []

func _get_next_triangle():

    var coords: Array = []
    var colors: Array = []

    var is_frame_end: bool = false
    var frame_end_value_count: int = 0

    for _i in range(3):

        var current_result_value: int = wacc_module.call_function("wacc")

        # Workaround for Linux apparently not zero-ing full 64-bit result
        # which results in top 32-bits not being zero which breaks my checks.
        #
        # TODO: Handle return values in WasmEngine properly.
        current_result_value =  current_result_value & 0xffffffff

        if current_result_value == _FRAME_END_VALUE:
            frame_end_value_count+=1

        coords.append(self.coord_from_int(current_result_value))
        colors.append(self.color_from_int(current_result_value))

    is_frame_end = (frame_end_value_count == 3)

    return {'coords': coords, 'colors': colors, 'is_frame_end': is_frame_end}


const MAX_TRIANGLES_PER_CALL: int = 256 # TODO: Figure out a reasonable value.

var _first_triangle_read: bool = true
var _frame_mode: bool = false

func _reset_state():

    self._first_triangle_read = true
    self._frame_mode = false


func _draw():

    if not wacc_module:
        return


    self._accumulated_triangles.clear()

    # TODO: Accumulate the triangles outside the `_draw()` function.
    while true: # TODO: Implement WASM processing limits: e.g. time, memory, calls.
        var current_triangle = self._get_next_triangle()

        # If the first triangle is a frame end marker then we
        # switch into multi-triangle-per-draw frame-based mode.
        #
        # This might be an unnecessary complication but it enables simple
        # examples to work correctly even when they don't support frame-mode
        # which is desirable in light of the project goals.
        if self._first_triangle_read:
            self._first_triangle_read = false

            if current_triangle.is_frame_end:
                self._frame_mode = true
                continue

        if self._frame_mode and current_triangle.is_frame_end:
            break

        self._accumulated_triangles.append(current_triangle)

        if not self._frame_mode:
            break

        if (self._accumulated_triangles.size() >= MAX_TRIANGLES_PER_CALL):
            push_warning("Hit MAX_TRIANGLES_PER_CALL limit of %d." % MAX_TRIANGLES_PER_CALL)
            break


    for current_triangle in self._accumulated_triangles:

        self.draw_primitive(current_triangle.coords, current_triangle.colors, [])

        # TODO: Look further at use of `draw_polygon()` for antialiasing (check "W" draws right).


# TODO: Would this be better in the/a "Viewer" parent?

# TODO: Figure out if there's a "better" way to handle update timing?
var _elapsed_time_seconds: float = 0.0

# TODO: Consider making standard update rate 1.5 or 2.0 FPS? Accessibility concerns?
const _update_rate_fps: float = 1.0

func _process(delta: float) -> void:

    self._elapsed_time_seconds += delta

    if self._elapsed_time_seconds >= (1.0 / _update_rate_fps):
        self.update()
        self._elapsed_time_seconds = 0
