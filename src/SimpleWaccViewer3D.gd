extends Spatial

onready var UI_BOX: Spatial = self.find_node("CSGBox", true, false)


# Note: Use of export/setting here seems to require that it doesn't get used
#       until a `ViewportContainer` parent of an instanced scene emits a
#       `ready` signal.
#
#       Otherwise the instanced scene doesn't seem to be completely loaded
#       so the property setting results in a null value error. (So it seems
#       that the `ready` signal child propagation doesn't work in the same
#       way if there are child nodes not directly in the scene?)
#
#       See `TestTextureRect._on_ViewportContainer_ready()` for example use.
#
export var wacc_texture: Texture setget _set_wacc_texture

func _set_wacc_texture(the_texture):

    wacc_texture = the_texture

    UI_BOX.material.albedo_texture = wacc_texture


func _ready():

    $"AnimationPlayer".play("BoxRotate", -1, 0.25)
