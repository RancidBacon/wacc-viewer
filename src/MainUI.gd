extends Control

onready var UI_WACC_VIEWER: Control = self.find_node("WaccViewer2D", true, false)


func load_wacc_from_file(wacc_file_path: String):

    UI_WACC_VIEWER.wacc_file_path = wacc_file_path


func _ready() -> void:

    var err = get_tree().connect("files_dropped", self, "_on_files_dropped")
    if err:
        push_warning("Could not connect to file drag & drop signal.")



func _on_files_dropped(files: PoolStringArray, _screen: int):

    var dropped_file_path: String = files[0]

    if dropped_file_path.get_extension() in ["wasm"]:
        self.load_wacc_from_file(dropped_file_path)



func _on_FileOpenDialog_file_selected(path: String) -> void:

    self.load_wacc_from_file(path)
