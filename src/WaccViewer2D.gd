#
#
#

extends Control


export var wacc_file_path: String setget _set_wacc_file_path


# If the rendering node layout seems convoluted, that's because it is... :/
#
# There is no straight-forward way AFAICT to retrieve the visual content[0] of a
# control/CanvasItem directly without use of a `Viewport`.
#
# [0] By visual content I mean including the result of `draw_*()` calls.
#
# The `.texture` property of a `TextureRect` is for *setting* a texture to be
# displayed--it's *not* a texture that represents the visual content of the `CanvasItem`.
#
# While inconvenient, this does seem "logical" once the implementation (as I
# understand it) is taken into account--the "drawing" gets stored as a bunch
# of drawing commands specific to the `CanvasItem` (e.g. see `VisualServer.canvas_item_add_polygon()`.
#
# So it's not like the `CanvasItem` is drawn first and *then* uploaded. (Although
# presumably that's what happens when individual pixels are set for an `Image`.)
#
# Our `TextureRect` child may be unnecessary as we could make ourselves extend
# from `TextureRect` rather than `Control` but having a separate `TextureRect` Node
# does make the design a little more explicit--and enables `get_texture_rect()`
# function implementation to not duplicate e.g. the viewport.
#
# In addition, note that we (the Viewer class) can be made non-visible but the
# texture is still updated correctly.
#
onready var TARGET_VIEWPORT: Viewport = self.find_node("Viewport", true, false)
onready var WACC_RENDERER: TextureRect = TARGET_VIEWPORT.find_node("WaccRenderer", true, false)
onready var OUR_TEXTURE_RECT: TextureRect = self.find_node("OurTextureRect", true, false)


var _wacc_loader: WaccLoader = WaccLoader.new()


func _set_wacc_file_path(file_path):
    wacc_file_path = file_path

    if not _wacc_loader:
        push_warning("No _wacc_loader so can't load file.")
        return

    WACC_RENDERER.wacc_module = _wacc_loader._load_valid_wacc(self.wacc_file_path)

    if not WACC_RENDERER.wacc_module:
        push_warning("Couldn't load WACC.")
        # TODO: Handle error.
        return


func _ready():

    # Note: We don't have the `ViewportTexture` set as part of the `TextureRect`
    #       configuration because the IDE(?) incorrectly sets the value as a
    #       relative rather than absolute path.
    #
    #       See docs for `ViewportTexture.viewport_path`.

    # Note: `get_texture()` not `texture` because `Viewport` doesn't have `texture` property!
    OUR_TEXTURE_RECT.texture = TARGET_VIEWPORT.get_texture()



func get_texture_rect():
    #
    # Provides a way to retrieve the rendered WACC texture for display in other
    # contexts (e.g. 3D).
    #

    # Note: Can't return just the `ViewportTexture` because the required "vertical flip" flag
    #       is a property of the `TextureRect` not the `ViewportTexture`. :/

    # Note: It turns out that there is also a "vertical flip" property on the
    #       viewport itself, so that might be an option although the combination
    #       of places we want to use the texture still might not be compatible with
    #       using that.

    return OUR_TEXTURE_RECT.duplicate()
