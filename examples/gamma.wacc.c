#include <stdint.h>

const uint32_t SENTINEL_VALUE = 0xffffffff;

uint32_t values[] = {
  0x0000ffff, 0xffffffff, 0x00ffffff,
  0x0000ffff, 0xffffffff, 0xff00ffff,
  0x0000ffff, 0x8080ffff, 0xff00ffff,
  SENTINEL_VALUE, SENTINEL_VALUE, SENTINEL_VALUE
};


uint32_t wacc() {

    static int index = 0;

    if ((index % 3) == 0) {

      if ((values[index] == SENTINEL_VALUE)
          && (values[index+1] == SENTINEL_VALUE)
          && (values[index+2] == SENTINEL_VALUE)) {
           index = 0;
         }

    }

    return values[index++];

}
