#include <stdint.h>

const uint32_t SENTINEL_VALUE = 0xffffffff;
const uint32_t FRAME_END_VALUE = 0x00000000;

uint32_t values[] = {

  // When the first triangle returned is the frame end marker
  // it indicates we want to be displayed in "frame mode".
  //
  // This marker is automatically skipped by the viewer
  // when initially returned and frame mode is then enabled.
  //
  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

    0x0000b15d, 0x007fb15d, 0x7f7fb15d, // PolygonA1
    0x0000b15d, 0x7f00b15d, 0x7f7fb15d, // PolygonA2

  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

    0x0000b15d, 0x007fb15d, 0x7f7fb15d, // PolygonA1
    0x0000b15d, 0x7f00b15d, 0x7f7fb15d, // PolygonA2
    0x7f00b15d, 0x7f7fb15d, 0xff7fb15d, // PolygonB1
    0x7f00b15d, 0xff00b15d, 0xff7fb15d, // PolygonB2

  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

    0x0000b15d, 0x007fb15d, 0x7f7fb15d, // PolygonA1
    0x0000b15d, 0x7f00b15d, 0x7f7fb15d, // PolygonA2
    0x7f00b15d, 0x7f7fb15d, 0xff7fb15d, // PolygonB1
    0x7f00b15d, 0xff00b15d, 0xff7fb15d, // PolygonB2
    0x007fabb6, 0x00ffabb6, 0x7fffabb6, // PolygonC1
    0x007fabb6, 0x7f7fabb6, 0x7fffabb6, // PolygonC2

  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

    0x0000b15d, 0x007fb15d, 0x7f7fb15d, // PolygonA1
    0x0000b15d, 0x7f00b15d, 0x7f7fb15d, // PolygonA2
    0x7f00b15d, 0x7f7fb15d, 0xff7fb15d, // PolygonB1
    0x7f00b15d, 0xff00b15d, 0xff7fb15d, // PolygonB2
    0x007fabb6, 0x00ffabb6, 0x7fffabb6, // PolygonC1
    0x007fabb6, 0x7f7fabb6, 0x7fffabb6, // PolygonC2
    0x7f7ff551, 0x7ffff551, 0xfffff551, // PolygonD1
    0x7f7ff551, 0xff7ff551, 0xfffff551, // PolygonD2

  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,


    0x0000b15d, 0x007fb15d, 0x7f7fb15d, // PolygonA1
    0x0000b15d, 0x7f00b15d, 0x7f7fb15d, // PolygonA2
    0x0000ffff, 0x3f00ffff, 0x1f7fffff, // PolygonA3
    0x3f00ffff, 0x7f00ffff, 0x5f7fffff, // PolygonA4
    0x7f00b15d, 0x7f7fb15d, 0xff7fb15d, // PolygonB1
    0x7f00b15d, 0xff00b15d, 0xff7fb15d, // PolygonB2
    0x007fabb6, 0x00ffabb6, 0x7fffabb6, // PolygonC1
    0x007fabb6, 0x7f7fabb6, 0x7fffabb6, // PolygonC2
    0x7f7ff551, 0x7ffff551, 0xfffff551, // PolygonD1
    0x7f7ff551, 0xff7ff551, 0xfffff551, // PolygonD2

  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

    0x0000b15d, 0x007fb15d, 0x7f7fb15d, // PolygonA1
    0x0000b15d, 0x7f00b15d, 0x7f7fb15d, // PolygonA2
    0x0000ffff, 0x3f00ffff, 0x1f7fffff, // PolygonA3
    0x3f00ffff, 0x7f00ffff, 0x5f7fffff, // PolygonA4
    0x7f00b15d, 0x7f7fb15d, 0xff7fb15d, // PolygonB1
    0x7f00b15d, 0xff00b15d, 0xff7fb15d, // PolygonB2
    0xbf00ffff, 0xff7fffff, 0x7f7fffff, // PolygonB3
    0x007fabb6, 0x00ffabb6, 0x7fffabb6, // PolygonC1
    0x007fabb6, 0x7f7fabb6, 0x7fffabb6, // PolygonC2
    0x7f7ff551, 0x7ffff551, 0xfffff551, // PolygonD1
    0x7f7ff551, 0xff7ff551, 0xfffff551, // PolygonD2

  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

    0x0000b15d, 0x007fb15d, 0x7f7fb15d, // PolygonA1
    0x0000b15d, 0x7f00b15d, 0x7f7fb15d, // PolygonA2
    0x0000ffff, 0x3f00ffff, 0x1f7fffff, // PolygonA3
    0x3f00ffff, 0x7f00ffff, 0x5f7fffff, // PolygonA4
    0x7f00b15d, 0x7f7fb15d, 0xff7fb15d, // PolygonB1
    0x7f00b15d, 0xff00b15d, 0xff7fb15d, // PolygonB2
    0xbf00ffff, 0xff7fffff, 0x7f7fffff, // PolygonB3
    0x007fabb6, 0x00ffabb6, 0x7fffabb6, // PolygonC1
    0x007fabb6, 0x7f7fabb6, 0x7fffabb6, // PolygonC2
    0x00bfffff, 0x7f7fffff, 0x7fffffff, // PolygonC3
    0x7f7ff551, 0x7ffff551, 0xfffff551, // PolygonD1
    0x7f7ff551, 0xff7ff551, 0xfffff551, // PolygonD2

  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

    0x0000b15d, 0x007fb15d, 0x7f7fb15d, // PolygonA1
    0x0000b15d, 0x7f00b15d, 0x7f7fb15d, // PolygonA2
    0x0000ffff, 0x3f00ffff, 0x1f7fffff, // PolygonA3
    0x3f00ffff, 0x7f00ffff, 0x5f7fffff, // PolygonA4
    0x7f00b15d, 0x7f7fb15d, 0xff7fb15d, // PolygonB1
    0x7f00b15d, 0xff00b15d, 0xff7fb15d, // PolygonB2
    0xbf00ffff, 0xff7fffff, 0x7f7fffff, // PolygonB3
    0x007fabb6, 0x00ffabb6, 0x7fffabb6, // PolygonC1
    0x007fabb6, 0x7f7fabb6, 0x7fffabb6, // PolygonC2
    0x00bfffff, 0x7f7fffff, 0x7fffffff, // PolygonC3
    0x7f7ff551, 0x7ffff551, 0xfffff551, // PolygonD1
    0x7f7ff551, 0xff7ff551, 0xfffff551, // PolygonD2
    0x7fbfffff, 0xff7fffff, 0xffffffff, // PolygonD3

  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,


    0x0000b15d, 0x007fb15d, 0x7f7fb15d, // PolygonA1
    0x0000b15d, 0x7f00b15d, 0x7f7fb15d, // PolygonA2
    0x0000ffff, 0x3f00ffff, 0x1f7fffff, // PolygonA3
    0x3f00ffff, 0x7f00ffff, 0x5f7fffff, // PolygonA4
    0x7f00b15d, 0x7f7fb15d, 0xff7fb15d, // PolygonB1
    0x7f00b15d, 0xff00b15d, 0xff7fb15d, // PolygonB2
    0xbf00ffff, 0xff7fffff, 0x7f7fffff, // PolygonB3
    0x007fabb6, 0x00ffabb6, 0x7fffabb6, // PolygonC1
    0x007fabb6, 0x7f7fabb6, 0x7fffabb6, // PolygonC2
    0x00bfffff, 0x7f7fffff, 0x7fffffff, // PolygonC3
    0x7f7ff551, 0x7ffff551, 0xfffff551, // PolygonD1
    0x7f7ff551, 0xff7ff551, 0xfffff551, // PolygonD2
    0x7fbfffff, 0xff7fffff, 0xffffffff, // PolygonD3

  // Because we're looping through our values, instead
  // of having a frame end marker here we let the
  // marker at the beginning of our data do the job.
  //
  // FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

  SENTINEL_VALUE, SENTINEL_VALUE, SENTINEL_VALUE
};


uint32_t wacc() {

    static int index = 0;

    if ((index % 3) == 0) {

      if ((values[index] == SENTINEL_VALUE)
          && (values[index+1] == SENTINEL_VALUE)
          && (values[index+2] == SENTINEL_VALUE)) {
           index = 0;
         }

    }

    return values[index++];

}
