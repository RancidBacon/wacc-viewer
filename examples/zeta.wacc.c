#include <stdint.h>

const uint32_t SENTINEL_VALUE = 0xffffffff;
const uint32_t FRAME_END_VALUE = 0x00000000;

uint32_t values[] = {

  // When the first triangle returned is the frame end marker
  // it indicates we want to be displayed in "frame mode".
  //
  // This marker is automatically skipped by the viewer
  // when initially returned and frame mode is then enabled.
  //
  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

  0x0000ffff, 0xffffffff, 0x00ffffff,
  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,
  0x0000ffff, 0xffffffff, 0xff00ffff,
  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,
  0x0000ffff, 0x8080fc00, 0xff00ffff,
  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,
  0x00ffffff, 0xffffffff, 0x8080801f,
  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,
  0x00008000, 0x00ff8000, 0xff800000,
  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

  0x0000ffff, 0xffffffff, 0x00ffffff,
  0x0000ffff, 0xffffffff, 0xff00ffff,
  0x0000ffff, 0x8080fc00, 0xff00ffff,
  0x00ffffff, 0xffffffff, 0x8080801f,
  0x00008000, 0x00ff8000, 0xff800000,

  // Because we're looping through our values, instead
  // of having a frame end marker here we let the
  // marker at the beginning of our data do the job.
  //
  // FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

  SENTINEL_VALUE, SENTINEL_VALUE, SENTINEL_VALUE
};


uint32_t wacc() {

    static int index = 0;

    if ((index % 3) == 0) {

      if ((values[index] == SENTINEL_VALUE)
          && (values[index+1] == SENTINEL_VALUE)
          && (values[index+2] == SENTINEL_VALUE)) {
           index = 0;
         }

    }

    return values[index++];

}
