(module
 (table 0 anyfunc)
 (memory $0 1)
 (data (i32.const 12) "\ff\ff\ff\ff")
 (data (i32.const 16) "\00\00\00\00")
 (data (i32.const 32) "\00\00\00\00\00\00\00\00\00\00\00\00\ff\ff\00\00\ff\ff\ff\ff\ff\ff\ff\00\00\00\00\00\00\00\00\00\00\00\00\00\ff\ff\00\00\ff\ff\ff\ff\ff\ff\00\ff\00\00\00\00\00\00\00\00\00\00\00\00\ff\ff\00\00\00\fc\80\80\ff\ff\00\ff\00\00\00\00\00\00\00\00\00\00\00\00\ff\ff\ff\00\ff\ff\ff\ff\1f\80\80\80\00\00\00\00\00\00\00\00\00\00\00\00\00\80\00\00\00\80\ff\00\00\00\80\ff\00\00\00\00\00\00\00\00\00\00\00\00\ff\ff\00\00\ff\ff\ff\ff\ff\ff\ff\00\ff\ff\00\00\ff\ff\ff\ff\ff\ff\00\ff\ff\ff\00\00\00\fc\80\80\ff\ff\00\ff\ff\ff\ff\00\ff\ff\ff\ff\1f\80\80\80\00\80\00\00\00\80\ff\00\00\00\80\ff\ff\ff\ff\ff\ff\ff\ff\ff\ff\ff\ff\ff")
 (export "memory" (memory $0))
 (export "wacc" (func $wacc))
 (func $wacc (; 0 ;) (result i32)
  (local $0 i32)
  (local $1 i32)
  (block $label$0
   (br_if $label$0
    (i32.rem_s
     (tee_local $1
      (i32.load offset=236
       (i32.const 0)
      )
     )
     (i32.const 3)
    )
   )
   (br_if $label$0
    (i32.ne
     (i32.load
      (i32.add
       (tee_local $0
        (i32.shl
         (get_local $1)
         (i32.const 2)
        )
       )
       (i32.const 32)
      )
     )
     (i32.const -1)
    )
   )
   (br_if $label$0
    (i32.ne
     (i32.load
      (i32.add
       (get_local $0)
       (i32.const 36)
      )
     )
     (i32.const -1)
    )
   )
   (br_if $label$0
    (i32.ne
     (i32.load
      (i32.add
       (i32.shl
        (get_local $1)
        (i32.const 2)
       )
       (i32.const 40)
      )
     )
     (i32.const -1)
    )
   )
   (set_local $1
    (i32.const 0)
   )
   (i32.store offset=236
    (i32.const 0)
    (i32.const 0)
   )
  )
  (i32.store offset=236
   (i32.const 0)
   (i32.add
    (get_local $1)
    (i32.const 1)
   )
  )
  (i32.load
   (i32.add
    (i32.shl
     (get_local $1)
     (i32.const 2)
    )
    (i32.const 32)
   )
  )
 )
)
