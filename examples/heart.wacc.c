#include <stdint.h>

const uint32_t SENTINEL_VALUE = 0xffffffff;
const uint32_t FRAME_END_VALUE = 0x00000000;

uint32_t values[] = {

  // When the first triangle returned is the frame end marker
  // it indicates we want to be displayed in "frame mode".
  //
  // This marker is automatically skipped by the viewer
  // when initially returned and frame mode is then enabled.
  //
  FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

    0x0000f551, 0x00ffb15d, 0xffff8000, // PolygonD1
    0x0000f551, 0xff00f551, 0xffff8000, // PolygonD2
    0x0000a0d3, 0x003fa0d3, 0x3f00a0d3, // PolygonD3
    0x3f00a0d3, 0x7f1fa0d3, 0x7f00a0d3, // PolygonD4
    0xc000a0d3, 0x7f1fa0d3, 0x7f00a0d3, // PolygonD5
    0xff00a0d3, 0xff3fa0d3, 0xc000a0d3, // PolygonD6
    0x003fa0d3, 0x00ffa0d3, 0x7fffa0d3, // PolygonD7
    0xff3fa0d3, 0xffffa0d3, 0x7fffa0d3, // PolygonD8

  // Because we're looping through our values, instead
  // of having a frame end marker here we let the
  // marker at the beginning of our data do the job.
  //
  // FRAME_END_VALUE, FRAME_END_VALUE, FRAME_END_VALUE,

  SENTINEL_VALUE, SENTINEL_VALUE, SENTINEL_VALUE
};


// Color sequences used to replace colors in particular frames.
const int COLOR0_LOOKUP_SIZE = 5;
const color0_lookup[COLOR0_LOOKUP_SIZE] = {
  0xb15d,
  0xa919,
  0xa4f4,
  0x9cb0,
  0x948c,
};

const int COLOR1_LOOKUP_SIZE = 3;
const color1_lookup[COLOR1_LOOKUP_SIZE] = {
  //  0xf656,
  0xf615,
  //  0xf5f4,
  0xf5b3,
  0xf572,
};


uint32_t wacc() {

    static int index = 0;
    static uint32_t call_count = 0;
    static uint32_t loop_count = 0;


    if ((index % 3) == 0) {

      if ((values[index] == SENTINEL_VALUE)
          && (values[index+1] == SENTINEL_VALUE)
          && (values[index+2] == SENTINEL_VALUE)) {
           index = 0;
           loop_count++;
         }

    }


    uint32_t result = values[index++];

    if (index == 5) {
      uint32_t color0 = color0_lookup[loop_count%COLOR0_LOOKUP_SIZE];
      result = (result & 0xffff0000) | color0;
    } else if (index == 8) {
      if ((loop_count % 20 > 16) ) {
        uint32_t color1 = color1_lookup[loop_count%COLOR1_LOOKUP_SIZE];
        result = (result & 0xffff0000) | color1;
      }
    }


    call_count++;

    return result;

}
