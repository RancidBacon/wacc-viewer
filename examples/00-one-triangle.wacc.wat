(module
 (table 0 anyfunc)
 (memory $0 1)
 (data (i32.const 12) "\ff\ff\00\00\ff\ff\ff\ff\ff\ff\ff\00")
 (export "memory" (memory $0))
 (export "wacc" (func $wacc))
 (func $wacc (; 0 ;) (result i32)
  (local $0 i32)
  (i32.store offset=24
   (i32.const 0)
   (i32.add
    (tee_local $0
     (i32.load offset=24
      (i32.const 0)
     )
    )
    (i32.const 1)
   )
  )
  (i32.load
   (i32.add
    (i32.shl
     (i32.rem_s
      (get_local $0)
      (i32.const 3)
     )
     (i32.const 2)
    )
    (i32.const 12)
   )
  )
 )
)
