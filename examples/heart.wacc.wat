(module
 (table 0 anyfunc)
 (memory $0 1)
 (data (i32.const 12) "\ff\ff\ff\ff")
 (data (i32.const 16) "\00\00\00\00")
 (data (i32.const 32) "\00\00\00\00\00\00\00\00\00\00\00\00Q\f5\00\00]\b1\ff\00\00\80\ff\ffQ\f5\00\00Q\f5\00\ff\00\80\ff\ff\d3\a0\00\00\d3\a0?\00\d3\a0\00?\d3\a0\00?\d3\a0\1f\7f\d3\a0\00\7f\d3\a0\00\c0\d3\a0\1f\7f\d3\a0\00\7f\d3\a0\00\ff\d3\a0?\ff\d3\a0\00\c0\d3\a0?\00\d3\a0\ff\00\d3\a0\ff\7f\d3\a0?\ff\d3\a0\ff\ff\d3\a0\ff\7f\ff\ff\ff\ff\ff\ff\ff\ff\ff\ff\ff\ff")
 (data (i32.const 152) "\05\00\00\00")
 (data (i32.const 160) "]\b1\00\00\19\a9\00\00\f4\a4\00\00\b0\9c\00\00\8c\94\00\00")
 (data (i32.const 180) "\03\00\00\00")
 (data (i32.const 184) "\15\f6\00\00\b3\f5\00\00r\f5\00\00")
 (export "memory" (memory $0))
 (export "wacc" (func $wacc))
 (func $wacc (; 0 ;) (result i32)
  (local $0 i32)
  (local $1 i32)
  (block $label$0
   (block $label$1
    (block $label$2
     (br_if $label$2
      (i32.rem_s
       (tee_local $0
        (i32.load offset=196
         (i32.const 0)
        )
       )
       (i32.const 3)
      )
     )
     (br_if $label$2
      (i32.ne
       (i32.load
        (i32.add
         (tee_local $1
          (i32.shl
           (get_local $0)
           (i32.const 2)
          )
         )
         (i32.const 32)
        )
       )
       (i32.const -1)
      )
     )
     (br_if $label$2
      (i32.ne
       (i32.load
        (i32.add
         (get_local $1)
         (i32.const 36)
        )
       )
       (i32.const -1)
      )
     )
     (br_if $label$1
      (i32.eq
       (i32.load
        (i32.add
         (i32.shl
          (get_local $0)
          (i32.const 2)
         )
         (i32.const 40)
        )
       )
       (i32.const -1)
      )
     )
    )
    (i32.store offset=196
     (i32.const 0)
     (i32.add
      (get_local $0)
      (i32.const 1)
     )
    )
    (set_local $1
     (i32.load
      (i32.add
       (i32.shl
        (get_local $0)
        (i32.const 2)
       )
       (i32.const 32)
      )
     )
    )
    (block $label$3
     (br_if $label$3
      (i32.eq
       (get_local $0)
       (i32.const 7)
      )
     )
     (br_if $label$0
      (i32.ne
       (get_local $0)
       (i32.const 4)
      )
     )
     (set_local $1
      (i32.or
       (i32.load
        (i32.add
         (i32.shl
          (i32.rem_u
           (i32.load offset=204
            (i32.const 0)
           )
           (i32.const 5)
          )
          (i32.const 2)
         )
         (i32.const 160)
        )
       )
       (i32.and
        (get_local $1)
        (i32.const -65536)
       )
      )
     )
     (br $label$0)
    )
    (br_if $label$0
     (i32.lt_u
      (i32.rem_u
       (tee_local $0
        (i32.load offset=204
         (i32.const 0)
        )
       )
       (i32.const 20)
      )
      (i32.const 17)
     )
    )
    (set_local $1
     (i32.or
      (i32.load
       (i32.add
        (i32.shl
         (i32.rem_u
          (get_local $0)
          (i32.const 3)
         )
         (i32.const 2)
        )
        (i32.const 184)
       )
      )
      (i32.and
       (get_local $1)
       (i32.const -65536)
      )
     )
    )
    (br $label$0)
   )
   (i32.store offset=196
    (i32.const 0)
    (i32.const 1)
   )
   (i32.store offset=204
    (i32.const 0)
    (i32.add
     (i32.load offset=204
      (i32.const 0)
     )
     (i32.const 1)
    )
   )
   (set_local $1
    (i32.load offset=32
     (i32.const 0)
    )
   )
  )
  (i32.store offset=200
   (i32.const 0)
   (i32.add
    (i32.load offset=200
     (i32.const 0)
    )
    (i32.const 1)
   )
  )
  (get_local $1)
 )
)
